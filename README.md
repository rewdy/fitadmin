fitadmin
==

This is a modified version of the Drupal default "Seven" admin theme.

The reason it was created was to add a region at the top where a menu could go. This way, there can be a menu in the admin theme but the Toolbar module does not have to be turned on.

Some colors and easily configurable using the style.less file. Be sure to compile it to style.css in order to see the changes.

-Andrew Meyer
 andrew@rewdy.com
